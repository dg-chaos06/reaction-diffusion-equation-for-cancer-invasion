function [ C ] = Implicit_diffusion(A,d)
%Implicit Diffusion: A is the matrix you want to diffuse and d is the
%stuff you put in the diffusion matrix.
%   Detailed explanation goes here

%creating the B matrix

endsat = numel(A);
B = zeros(endsat,endsat);

for n = 1:endsat
    
    
    left = n-1;
    middle = n;
    right = n + 1;
    
    if left == 0, left = endsat; end
    if right == endsat + 1, right = 1; end
    
    B(middle,middle) = 1 + 2*d(n);
    B(middle,left) = -d(n);
    B(middle,right) = -d(n);
    
end

C = B\A;


end

