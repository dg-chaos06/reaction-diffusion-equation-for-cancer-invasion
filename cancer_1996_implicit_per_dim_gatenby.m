%Diffusion uses explicit Euler method
%Reaction uses explicit Euler method
clear
close all
frame = 0; %frame for my movie
movie_out = figure;

dt = 1e4;
dx = 0.01;

Length = 2;

n = Length/dx;

KN = 5e7; %carrying capacities
KC = 5e7;
KT = 5e7;
rN = 1e-6;
rC = 1e-6;
rT = 1e-6; %immune cell or therapy growth rate
DC = 2e-10;
DH = 5e-6;  %Let's assume both the hydrogen and the 'drug' diffuse at the same rate
DT = 5e-6; %I assumed the drug or therapy diffuses as the same rate as H ions
rH = 2.2e-17; % increase in H due to cancer
dH = 0.8e-4; %decay of H
dT = 0; %decay of therapy/drug
dNH = 1; %Normal cell death by H
dCH = 0.2; %cancer cell death by H
dNT = 0; %Therapy kills normal cells
dCT = 5e2; %Therapy kills cancers
aNC = 0.8; % competition terms
aCN = 0.2;
H0 = rH*KC/dH;
T0 = 5e7; %therapy normalization. Change depending on whether this is immune or drug.
Cmut = 3; %Cancer mutation

%nondimensional constants

del1 = (dNH/dH)*(rH/rN)*KC;
rho2 = rC/rN;
DEL2 = DC/DH;
del3 = dH/rN;

N = 1*ones(n,1)*KN; %nondimensionalized normal cells

C = 0*ones(n,1); %nondimensionalized cancer cells

H = zeros(n,1); %nondimensionalized H+ concentration

T = zeros(n,1); %non-timensionalized therapy crap

mutT = zeros(n,1); %Mutation matrix

% Normal_N1 = 0.5*K1*ones(1,n); %A represents the current cytoplasm surface
% Normal_N1(end) = 0;
% Cancer_N2 = zeros(1,n);
% Cancer_N2(end) = 0.5*K2;
% Hydroexcess_L = zeros(1,n);
% Hydroexcess_L(end) = 1;

writerObj = VideoWriter('1996_per_implicit.avi');
open(writerObj); %Taking the video


for time = 1 : 5000
    
    %Event triggers
    
    
    if time == 1, C(n/8:n/4) = 0.1*KT; end
   % if time == 1, C(n*3/8:(n/4 + 3*n/8)) = 0.05*KT; end
    if time == 10, T(n/4:n/2) = 0*KT; end
 %   if time == 100, C(n/8:n/4) = 0.3*KT; end
    
    
    %Normal Cells
    
    for normapp = 1:20
        
        %no normal cell diffusion for now
        
        ReacetN = dt/20*(rN*N.*(1 - N./KN - aNC*C/KC) - dNT*T.*N - dNH*H.*N);
        
        N = N + ReacetN;
        
    end
    
    %Cancer Cells
    for canapprdiff = 1:20
        
        D = DC.*(1 - N/KN);
        d = dt/20*D/(dx^2); %the implicit diffusion constant
        
        C = Implicit_diffusion(C,d); %Diffuse
        
        
        
        C = C + dt/20*(rC*C.*(1 - C/KC - aCN*N/KN)  - dCT*exp(-mutT).*T.*C/(KT*KC) -  dCH*H.*C); %React. There is also a mutation term here.
        
    end
    
    %Cancer mutation
    
    
    
    
    %mutation clock
    
    if mod(time,10) == 0
        
        mutT = mutT + Cmut.*T.*C./(KT*KC);
        
    end
    
    for ind = 1:n
        
        if C(ind)/KC < 1e-7, mutT(ind) = 0; end
        
    end
    %Acid concentration
    
    d = DH*dt/(20*(dx^2))*ones(1,n); %Input for implicit diffusion. I am running twenty steps
    
    for acidapprdiff = 1:20
        
        H = Implicit_diffusion(H,d);
        H = H + dt/20*(rH*C - dH*H);
        
    end
    
    d = DT*dt/(20*(dx^2))*ones(1,n);
    
    for therapyapprdiff = 1:20
        
        %This is diffusing at the same rate as the H+ ions. May be it
        %should be slower?
        
        T = Implicit_diffusion(T,d);
        
        T = T + dt/20*(rT*T.*(1 - T./(KN*mean(N)/KT)) - dT*T); %The immune cells are dependent on how many healthy cells there are. healthy cells indirectly sustain immune cells.
        %The drug has some half life
        
    end
    %Video capture
    
    %Frame capture
    if time == 1 || time == 480 || time == 800 || time == 2700
        
        hold off
        plot(N/KN)
        hold on
        plot(C/KC,'r')
        plot(H/H0,'g')
        plot(T/T0,'k')
        
        axis([0, n + 1, 0, 1.5]);
        
        disptime = strcat('Time =  ',num2str(time*10000/86400),' days');
        text(15,1.3,disptime);
        legend('Normal Cells','Cancer Cells','Excess Acid','Immune Cells')
        
        if time == 1, saveas(movie_out,'immwincanclose1','png'); end
       if time == 480, saveas(movie_out,'immwincanclose2','png'); end
if time == 800, saveas(movie_out,'immwincanclose3','png'); end
      if time == 2700, saveas(movie_out,'immwincanclose4','png'); end  

        F = getframe(movie_out);
        writeVideo(writerObj,F);
        
    end
    
end

close(writerObj);

